import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const Header: React.FC<{}> = () => {
  return (
    <View style={styles.header}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>GENERATE STARS</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  titleContainer: {
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 25,
    fontWeight: '800',
    fontFamily: 'Montserrat',
    color: '#FFF',
  },
  header: {
    height: 170,
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#3d3d3d',
  },
});

export default Header;
