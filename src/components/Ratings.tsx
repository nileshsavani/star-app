import React from 'react';
import {View, StyleSheet, Platform, Text} from 'react-native';
import Star from './Star';

interface RatingsProps {
  ratingCounts: Array<number>;
  rating?: number;
}

const Ratings: React.FC<RatingsProps> = ({ratingCounts, rating}) => {
  const renderRatings = () =>
    ratingCounts.map((progress: number, index: number) => (
      <Star key={index} progress={progress} />
    ));

  return (
    <View style={styles.ratingContainer}>
      <View style={styles.wrapper}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>RATINGS</Text>
        </View>
        <View>
          <Text style={styles.ratingText}>{rating}/5</Text>
        </View>
        <View style={styles.starContainer}>{renderRatings()}</View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  ratingContainer: {
    flex: 1,
    backgroundColor: '#FFF',
    padding: 15,
    margin: 15,
    marginBottom: 0,
    ...Platform.select({
      android: {
        elevation: 1,
      },
      default: {
        shadowColor: 'rgba(0,0,0,0.5)',
        shadowOffset: {width: 5, height: 5},
        shadowOpacity: 0.2,
        shadowRadius: 20,
      },
    }),
  },
  wrapper: {
    backgroundColor: 'transparent',
  },
  titleContainer: {
    marginBottom: 0,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(233,233,233,0.7)',
  },
  title: {
    textAlign: 'center',
    marginBottom: 15,
    fontSize: 13,
    fontWeight: '700',
    fontFamily: 'Montserrat',
    color: '#3d3d3d',
  },
  starContainer: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  ratingText: {
    fontWeight: 'bold',
    marginTop: 20,
    fontFamily: 'Montserrat',
    color: '#f1c40f',
    fontSize: 20,
    textAlign: 'center',
  },
});

export default Ratings;
