import React from 'react';
import {View, StyleSheet} from 'react-native';

const Marker: React.FC<{}> = () => {
  return (
    <View style={styles.container}>
      <View style={styles.circle}></View>
    </View>
  );
};

const styles = StyleSheet.create({
  circle: {
    width: 30,
    height: 30,
    borderRadius: 30,
    borderWidth: 1,
    borderColor: '#f1c40f',
    backgroundColor: '#f1c40f',
  },
  container: {
    position: 'relative',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Marker;
