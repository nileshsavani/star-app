import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import LinearGradient from 'react-native-linear-gradient';

interface CardProps {
  onPress: () => void;
}
const Card: React.FC<CardProps> = ({onPress}) => {
  const colors = ['#7F7FD5', '#86A8E7'];
  return (
    <View style={styles.cardContainer}>
      <View style={styles.card}>
        <TouchableOpacity onPress={onPress} style={styles.buttonContainer}>
          <LinearGradient colors={colors} style={styles.buttonContainer}>
            <View style={styles.buttonContent}>
              <Text style={styles.text}>GENERATE RANDOM RATINGS</Text>
            </View>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  cardContainer: {
    marginTop: 85,
  },
  card: {
    flex: 1,
    borderRadius: 50,
    shadowColor: '#444',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 0.2,
    shadowRadius: 8,
    elevation: 1,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 20,
    paddingVertical: 20,
    backgroundColor: '#7F7FD5',
    position: 'relative',
  },
  text: {
    fontSize: 13,
    fontWeight: '700',
    fontFamily: 'Montserrat',
    color: '#fff',
    alignContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
  },
  buttonContainer: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    flex: 1,
    borderRadius: 50,
    shadowColor: '#444',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 0.2,
    shadowRadius: 8,
    elevation: 1,
    paddingVertical: 12,
  },
  buttonContent: {
    width: '100%',
    height: 100,
  },
});

export default Card;
