import React, {useState} from 'react';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import {View, StyleSheet, Text} from 'react-native';
import {Marker} from './';

interface SliderProps {
  onChange: (rating: number) => void;
  min: number;
  max: number;
}

const Slider: React.FC<SliderProps> = ({onChange, min, max}) => {
  const [value, setValue] = useState(0);

  const item = (index: number) => {
    return (
      <View key={index}>
        <Text style={checkActive(index) ? styles.active : styles.inactive}>
          {index}
        </Text>
        <Text style={checkActive(index) ? styles.line : {}}>
          {checkActive(index) ? '|' : ''}
        </Text>
      </View>
    );
  };

  const checkActive = (index: number) => {
    return index >= min && index <= value ? true : false;
  };

  const renderScale = () => {
    const items = [];
    for (let i = min; i <= max; i++) {
      items.push(item(i));
    }
    return items;
  };

  const onValueChange = (values: number[]) => {
    setValue(values[0]);
    onChange(values[0]);
  };

  return (
    <>
      <View style={styles.scaleContainer}>{renderScale()}</View>
      <View style={styles.container}>
        <MultiSlider
          min={min}
          max={max}
          values={[0]}
          onValuesChange={onValueChange}
          step={0.5}
          snapped={true}
          trackStyle={{backgroundColor: '#bdc3c7'}}
          selectedStyle={{backgroundColor: '#5e5e5e'}}
          allowOverlap={false}
          customMarker={Marker}
        />
      </View>
    </>
  );
};
const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  scaleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    bottom: -20,
    marginLeft: 40,
    marginRight: 40,
  },
  active: {
    textAlign: 'center',
    fontSize: 15,
    bottom: 10,
    color: '#5e5e5e',
  },
  inactive: {
    textAlignVertical: 'center',
    textAlign: 'center',
    fontWeight: 'normal',
    color: '#bdc3c7',
  },
  line: {
    fontSize: 10,
    textAlign: 'center',
  },
});

export default Slider;
