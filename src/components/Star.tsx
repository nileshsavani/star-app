import React from 'react';
import {StyleSheet, View, Image} from 'react-native';
import {STAR_IMAGE, STAR_IMAGE_SELECTED} from '../assets/images';

const STAR_SIZE = 30;

interface StarProps {
  progress?: number;
}

const Star: React.FC<StarProps> = ({progress = 0}) => {
  const filledStyle = [
    styles.absoluteStar,
    {width: STAR_SIZE * Number(progress / 100)},
  ];
  return (
    <>
      <View style={styles.starContainer}>
        <View style={styles.star}>
          <Image source={STAR_IMAGE} style={styles.star} />
        </View>
        <View style={filledStyle} removeClippedSubviews>
          <Image source={STAR_IMAGE_SELECTED} style={styles.star} />
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  starContainer: {
    flexDirection: 'row',
    margin: 10,
  },
  star: {
    width: STAR_SIZE,
    height: STAR_SIZE,
  },
  absoluteStar: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    overflow: 'hidden',
  },
});

export default Star;
