export {default as Card} from './Card';
export {default as Header} from './Header';
export {default as Star} from './Star';
export {default as Ratings} from './Ratings';
export {default as Slider} from './Slider';
export {default as Marker} from './Marker';
