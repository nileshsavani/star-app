import React, {useEffect, useState} from 'react';
import {SafeAreaView, StyleSheet, View, StatusBar} from 'react-native';
import {Header, Card, Ratings, Slider} from './components';
import {random} from 'lodash-es';

const App: React.FC<{}> = () => {
  const [rating, setRating] = useState<number>(0);
  const [ratingCounts, setRatingCounts] = useState([0, 0, 0, 0, 0]);
  useEffect(() => {
    StatusBar.setBarStyle('light-content');
  }, []);

  const onPress = () => {
    const newRating = Number(random(5, true).toFixed(2)); // generate random ratings inclusively 0 to 5 (floating value)
    setRating(newRating);
  };

  const onChange = (values: number) => setRating(values);

  // create ratings array using all over rating
  const createRatingCounts = (rating: number) => {
    let ratings = new Array(5).fill(0);
    let k = 0;
    while (rating) {
      if (rating < 1) {
        ratings[k] = Math.abs(Number(rating.toFixed(2)) * -100);
        return ratings;
      }
      rating -= 1;
      ratings[k] = 100;
      k++;
    }
    return ratings;
  };

  useEffect(() => {
    setRatingCounts(createRatingCounts(rating));
  }, [rating]);

  return (
    <View style={styles.container}>
      <SafeAreaView style={styles.safeAreaView}>
        <Header />
        <Card onPress={onPress} />
        <View style={styles.content}>
          <Ratings ratingCounts={ratingCounts} rating={rating} />
        </View>
        <Slider onChange={onChange} min={0} max={5} />
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  content: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  safeAreaView: {
    flex: 1,
  },
});

export default App;
