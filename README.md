# Rating App (react-native >= 0.60)

There are a few important prerequisites which should be prepared or answered before deployment:

## React Native support for ios

- You must know how to configure and run react native project for ios for react-native >= 0.60.
- install dependecies using `npm install`
- install pods using `cd ios && pod install`
- link dependecies using `react-native link`

## Available Scripts

In the project directory, you can run:

### `npm run ios` or `npm run android`

Run an app in iPhone simulator

### `npm run start`

Starts bundler. (by default it will starts when you run `npm run ios` or `npm run android`)

## In Action

![](inaction.gif)
